<?php

// run examples of sending emails, first you have to modify the API_KEY value into Transactional class
SendTransactional::send_emails();

/**
 * Class Transactional
 */
class Transactional
{
    // you can found the value into your Conectoo Account
    const API_KEY = 'API_KEY_VALUE';

    const API_URL = 'https://api.conectoo.com/v1/mail/send';

    private $curl;

    /**
     * Init curl and attach api key.
     */
    public function init_curl()
    {
        $this->curl = curl_init();

        $this->attach_api_key();
    }

    /**
     * Attach api key to current curl.
     */
    private function attach_api_key()
    {
        curl_setopt_array($this->curl, [
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . self::API_KEY,
            ],
        ]);
    }

    /**
     * @param array $parameters
     * @return mixed
     * Send Email to Conectoo API using API_URL
     */
    public function send(array $parameters)
    {
        $this->init_curl();

        curl_setopt_array($this->curl, [
            CURLOPT_URL => self::API_URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 1,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 600,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $parameters,
        ]);

        $response = curl_exec($this->curl);

        curl_close($this->curl);

        return $response;
    }
}

/**
 * Class SendTransactional
 */
class SendTransactional
{
    private static $transactional;

    /**
     * Sending example emails using mixed parameters.
     */
    public static function send_emails()
    {
        $content = "Hello World! <br/> 
        <strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";

        // success email example
        echo self::send('john_doe@example.com', 'Test Email', 'jane_doe@example.com',
            'Jane Doe', $content);

        // error response example
        echo self::send('john_doe', 'Test Email', 'jane_doe', 'Jane Doe', $content);

          // test email example
        echo self::send('john_doe@example.com', 'Test Email', 'jane_doe@example.com',
            'Jane Doe', $content, 'jane_doe@example.com', 1, 1, 'test', 1);

        // email example using cc list
        $cc_list = ['jane_doe_1@example.com', 'jane_doe_2@example.com'];

        echo self::send('john_doe@example.com', 'Test Email', 'jane_doe@example.com',
            'Jane Doe', $content, 'jane_doe@example.com', 0, 0, 'test', 0, $cc_list);

        // test email using attachments
        $attachments = [
            [
                'name' => 'test.png',
                'path' => 'test.png',
            ],
        ];

        echo self::send('john_doe@example.com', 'Test Email', 'jane_doe@example.com',
            'Jane Doe', $content, 'jane_doe@example.com', 0, 0, 'test', 0, [], [],
            $attachments);
    }

    /**
     * @param string $email
     * @param string $subject
     * @param string $sender_email
     * @param string $sender_name
     * @param string $message_content
     * @param string|null $sender_reply_to
     * @param int $track_open , accepted values: 1 or 0
     * @param int $track_click , accepted values: 1 or 0
     * @param string|null $labels
     * @param int $just_test , accepted values: 1 or 0
     * @param array $cc_list
     * @param array $bcc_list
     * @param array $attachments
     * Implementation of API parameters
     */
    public static function send(
        string $email,
        string $subject,
        string $sender_email,
        string $sender_name,
        string $message_content,
        string $sender_reply_to = null,
        int $track_open = 0,
        int $track_click = 0,
        string $labels = null,
        int $just_test = 0,
        array $cc_list = [],
        array $bcc_list = [],
        array $attachments = []
    ) {
        if (empty(self::$transactional)) {
            self::$transactional = new Transactional();
        }

        if (!empty($cc_list)) {
            $cc_list = json_encode($cc_list);
        }

        if (!empty($bcc_list)) {
            $bcc_list = json_encode($bcc_list);
        }

        $attachments_files = [];

        if (!empty($attachments)) {
            foreach ($attachments as $attachment) {
                if (false === file_exists($attachment['path'])) {
                    continue;
                }

                $attachments_files[] = [
                    'name' => $attachment['name'],
                    'file' => base64_encode($data),
                ];
            }

            $attachments_files = json_encode($attachments_files);
        }

        $parameters = [
            'email' => $email,
            'subject' => $subject,
            'sender_email' => $sender_email,
            'sender_name' => $sender_name,
            'message_content' => $message_content,
        ];

        self::attach_parameter_if_exists($parameters, $sender_reply_to, 'sender_reply_to');
        self::attach_parameter_if_exists($parameters, $track_open, 'track_open');
        self::attach_parameter_if_exists($parameters, $track_click, 'track_click');
        self::attach_parameter_if_exists($parameters, $labels, 'labels');
        self::attach_parameter_if_exists($parameters, $just_test, 'just_test');
        self::attach_parameter_if_exists($parameters, $cc_list, 'cc_list');
        self::attach_parameter_if_exists($parameters, $bcc_list, 'bcc_list');
        self::attach_parameter_if_exists($parameters, $attachments_files, 'attachments');

        return self::$transactional->send($parameters);
    }

    private static function attach_parameter_if_exists(&$parameters, $value, $key)
    {
        if (empty($value)) {
            return;
        }

        $parameters[$key] = $value;
    }
}
